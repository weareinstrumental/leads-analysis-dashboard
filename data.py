import logging
import boto3
import time
import numpy as np
import streamlit as st
import pandas as pd
import logging

from altair.vegalite.v4.schema.channels import X
from numpy.lib.arraysetops import isin
from numpy.ma.core import shape
from datetime import datetime
from boto3.dynamodb.conditions import Attr, Contains, Key
from streamlit.proto.DataFrame_pb2 import DataFrame

from env import TABLE_NAME,DYNAMODB,AttributeProfile

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())
client = boto3.client('logs')

def parse_logs(query: str, start: datetime, end: datetime, log_group: str):
    start_query_response = client.start_query(
    logGroupName=log_group,
    startTime=int(datetime.combine(start, datetime.min.time()).timestamp()),
    endTime=int(datetime.combine(end, datetime.max.time()).timestamp()),
    queryString=query,
    )
    query_id = start_query_response['queryId']
    response = None
    artist_list=[]
    while response == None or response['status'] == 'Running':
        # print('Waiting for query to complete ...')
        time.sleep(1)
        response = client.get_query_results(
            queryId=query_id
        )
    for item in response["results"]:
        artist_list.append({"timestamp": item[0]["value"], "artist_id": item[1]["value"]})
    return artist_list 


def load_data(prospect_data):
    data = pd.DataFrame(prospect_data)
    return data

def safe_int(value: int):
    if value:
        return int(value)
    return 0 

def format_datetime(when, timespec: str = None):
    """Format a datetime into ISO format, by default using seconds but allowing any timespec"""
    if timespec:
        return when.isoformat(timespec=timespec)
    else:
        return when.isoformat()

def query_with_pagination(query_params, attribute_profile=AttributeProfile.ALL_PROJECTED, enrich_item: callable = None):
    additional_params = build_additional_params(attribute_profile)

    more_pages = True
    item_count = 0
    page_count = 0
    consumed_capacity = {}
    while more_pages:
        response = get_table().query(**query_params, **additional_params)
        page_count += 1

        for item in response.get("Items", []):
            # if a callback is provided
            if enrich_item:
                item = enrich_item(item)

            item_count += 1
            yield item

        # handle pagination
        if response.get("LastEvaluatedKey"):
            logger.debug("Querying for next page: %s", response.get("LastEvaluatedKey"))
            additional_params["ExclusiveStartKey"] = response.get("LastEvaluatedKey")
        else:
            more_pages = False

        add_consumed_capacity(response, consumed_capacity)

    logger.info("Loaded %d items in %d pages", item_count, page_count)
    log_consumed_capacity(consumed_capacity)

def build_additional_params(attribute_profile: AttributeProfile):
    if attribute_profile and attribute_profile.value.get("ProjectionExpression"):
        attr_vals = attribute_profile.value
        additional_params = {
            "Select": attr_vals.get("Select", "ALL_ATTRIBUTES"),
            "ProjectionExpression": attr_vals.get("ProjectionExpression", ""),
            "ExpressionAttributeNames": attr_vals.get("ExpressionAttributeNames", {}),
            "ReturnConsumedCapacity": "INDEXES",
        }
        logger.debug("Built additional parameters from profile %s: %s", attribute_profile.name, additional_params)
    else:
        additional_params = {"ReturnConsumedCapacity": "INDEXES"}
        logger.debug("AttributeProfile not provided, or does not need additional parameters")

    return additional_params

def add_consumed_capacity(response: dict, consumed_capacity: dict):
    """Accumulate consumed capacity from the response

    Args:
        response (dict): [description]
        consumed_capacity (dict): [description]
    """
    capacity = response.get("ConsumedCapacity")
    if capacity:
        if "ReadCapacityUnits" in capacity:
            consumed_capacity.setdefault("ReadCapacityUnits", 0)
            consumed_capacity.setdefault("WriteCapacityUnits", 0)

            consumed_capacity["ReadCapacityUnits"] += capacity.get("ReadCapacityUnits", 0)
            consumed_capacity["WriteCapacityUnits"] += capacity.get("WriteCapacityUnits", 0)
        elif "CapacityUnits" in capacity:
            consumed_capacity.setdefault("CapacityUnits", 0)
            consumed_capacity["CapacityUnits"] += capacity.get("CapacityUnits", 0)

def log_consumed_capacity(consumed_capacity):
    if consumed_capacity:
        logger.debug("Consumed capacity: %s", consumed_capacity)

def get_table():
    return DYNAMODB.Table(TABLE_NAME)

  # info about playlists requiring an update

st.title("Instrumental Leads Analysis")


@st.cache
def fetch_data(data: list, attributes: str = None, id: str="artist_id"):
    id_list=[d[id] for d in data] 
    query_params = {
        "Select": "ALL_ATTRIBUTES",
        "KeyConditionExpression": Key("PK").eq("prospector#sp#artist-workflow") & Key("SK").begins_with("artist#sp#"),
        "FilterExpression": Contains(id_list, Attr("artist")),
    
    } 
    if attributes:
        query_params["ProjectionExpression"]= attributes
        del query_params["Select"]
    return dynamo(query_params=query_params)

@st.cache
def dynamo(query_params: dict):        
    result=[]
    for prospect in query_with_pagination(query_params):
        # do not allow records without score data
        if prospect.get("score_data"):
            prospect["social_score"] =  round(prospect["score_data"]["scores"]["social"]["social_score"])
            prospect["insta_engagement_rate"] = round(prospect["score_data"]["scores"]["social"]["data"]["instagram"]["engagement_rate"])
            prospect["tik_engagement_rate"] = round(prospect["score_data"]["scores"]["social"]["data"]["tiktok"]["engagement_rate"])
            prospect["insta_followers"] =  round(prospect["score_data"]["scores"]["social"]["data"]["instagram"]["followers"])
            prospect["tik_followers"] =  round(prospect["score_data"]["scores"]["social"]["data"]["tiktok"]["followers"])
            prospect["tik_video_views"] =  round(prospect["score_data"]["scores"]["social"]["data"]["tiktok"]["average_video_views"])
            prospect["50K_other"] = prospect["score_data"]["scores"]["social"]["other_socials"]["over_50K"]
            prospect["spotify_score"] =  round(prospect["score_data"]["scores"]["spotify"]["spotify_score"])
            prospect["monthly_listeners"] = int(prospect["score_data"]["scores"]["spotify"]["data"]["monthly_listeners"])
            prospect["track_popularity"] = int(prospect["score_data"]["scores"]["spotify"]["data"]["track_popularity"])
            prospect["artist_popularity"] = int(prospect["score_data"]["scores"]["spotify"]["data"]["artist_popularity"])
            prospect["spot_followers"] = int(prospect["score_data"]["scores"]["spotify"]["data"]["followers"])
            del prospect["score_data"]
        else:
            continue
        if prospect.get("prospect_status", {}):
            prospect["status"] = prospect.get("prospect_status").get("status")
            prospect["status_updated"] = prospect.get("prospect_status").get("updated")
            if prospect["status"] == "Not Suitable":
                prospect["reason"] = prospect["prospect_status"]["reason"]
            else:
                prospect["reason"] = None
        else:
            if prospect.get("score_data", {}):
                if prospect["social_score"] > 5 or prospect["spotify_score"] > 5:
                    prospect["status"]="Qualifying"
                    prospect["status_updated"] = prospect["added_at"]
        result.append(prospect)
    return result

@st.cache
def separate_manual(data:list):
    manual=[]
    tech=[]
    for lead in data:
        if lead["origin"] == "manual":
            manual.append(lead)
        else:
            tech.append(lead)
    return tech,manual


@st.cache
def signed_lost(start,end):
    signed =[]
    lost=[]
    query_params = {
        "Select": "ALL_ATTRIBUTES",
        "KeyConditionExpression": Key("PK").eq("prospector#sp#artist-workflow") & Key("SK").begins_with("artist#sp#"),
        "FilterExpression": Attr("prospect_status.updated").between(format_datetime(start), format_datetime(end)) & Attr("prospect_status.status").eq("Closed Lost") | Attr("prospect_status.status").eq("Deal Signed")
    }
    for row in dynamo(query_params):
        if row["prospect_status"]["status"] == "Closed Lost":
            lost.append(row)
        if row["prospect_status"]["status"] == "Deal Signed":
            signed.append(row)
    return signed, lost


@st.cache
def count_reasons(data: DataFrame):
    genre_not_a_fit = safe_int(data.groupby("reason").size().get("Genre not a fit", 0))
    wrong_language =  safe_int(data.groupby("reason").size().get("Wrong language", 0))
    fake_artist =  safe_int(data.groupby("reason").size().get("Fake Artist", 0))
    signed_to_a_major = safe_int(data.groupby("reason").size().get("Signed to a major", 0))
    singed_to_a_large_indie = safe_int(data.groupby("reason").size().get("Signed to a large indie", 0))
    too_old = safe_int(data.groupby("reason").size().get("Too Old"))
    back_catalogue_unavailable = safe_int(data.groupby("reason").size().get("Back Catalogue Unavailable", 0))
    frtyfve_artist = safe_int(data.groupby("reason").size().get("Frtyfve Artist", 0))
    poor_quality = safe_int(data.groupby("reason").size().get("Poor Quality", 0))
    score_too_low = safe_int(data.groupby("reason").size().get("Score too low", 0))
    not_read_for_tracking = safe_int(data.groupby("reason").size().get("Not ready for tracking", 0))
    already_in_pipeline = safe_int(data.groupby("reason").size().get("Already in Pipeline", 0))
    other = safe_int(data.groupby("reason").size().get("Other", 0))
    return [genre_not_a_fit, wrong_language, fake_artist, signed_to_a_major, singed_to_a_large_indie, too_old, back_catalogue_unavailable, frtyfve_artist, poor_quality,score_too_low,not_read_for_tracking,already_in_pipeline,other]


@st.cache
def format_stats(data: list):
    chart=[]
    for rows in data:
        name = rows["artist_name"]
        chart.append({
            "platform":"instagram",
            "engagement": round(float(rows["insta_engagement_rate"]), 2), 
            "followers": int(rows["insta_followers"]),
            "artist_name":name
            })
        chart.append({
            "platform":"tiktok",
            "engagement": round(float(rows["tik_engagement_rate"]), 2), 
            "followers": int(rows["tik_followers"]),
            "artist_name":name
            })
    return chart

def not_in(list1, list2):
    explode = []
    for item in list1:
        if item in list2:
            explode.append(0.1)
        else:
            explode.append(0)
    return tuple(explode)

@st.cache
def convert_df(df):
    # IMPORTANT: Cache the conversion to prevent computation on every rerun
    return df.to_csv().encode('utf-8')