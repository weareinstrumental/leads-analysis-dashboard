import boto3
from enum import Enum

LEADS_LOG_GROUP = "/aws/lambda/track-prospecting-prod-gsheetRecorder"
YESNO_LOG_GROUP = "/aws/lambda/artist-score-prod-transitionArtists"
LEADS_QUERY = "filter @message like /Appending (row|Lead)/ | parse message \"Appending Lead row: ['*', \" AS artist_id_v1 | parse message \"Appending row for artist * to leads sheet\" AS artist_id_v2 | display @timestamp, coalesce(artist_id_v1, artist_id_v2) as artist_id | sort @timestamp desc | limit 10000"
YES_QUERY = "filter @message like /Removed promoted artist/ | fields @message | parse message \"Removed promoted artist * from leads sheet\" AS artist_id | display @timestamp, artist_id | order by @timestamp | Limit 10000"
NO_QUERY = "filter @message like /Removed unsuitable artist / | fields @message | parse message \"Removed unsuitable artist from leads sheet *:*\" AS artist_id, remainder | display @timestamp, artist_id"
REJECTION_REASONS = [
    "Genre not a fit",
    "Wrong language",
    "Fake Artist",
    "Signed to a major",
    "Signed to a large indie",
    "Too Old",
    "Back Catalogue Unavailable",
    "Frtyfve Artist",
    "Poor Quality",
    "Score too low",
    "Not ready for tracking",
    "Already in Pipeline",
    "Other"]

TABLE_NAME = "Ingestion-prod"
DYNAMODB = boto3.resource("dynamodb", endpoint_url="https://dynamodb.eu-west-2.amazonaws.com")

class AttributeProfile(Enum):
    """Define profiles of attributes to retrieve from queries"""

    ALL = {"Select": "ALL_ATTRIBUTES"}
    ALL_PROJECTED = {"Select": "ALL_PROJECTED_ATTRIBUTES"}
    PLAYLIST_MINIMAL = {"Select": "SPECIFIC_ATTRIBUTES", "ProjectionExpression": "PK"}
    PLAYLIST_FOR_UPDATE = {
        "Select": "SPECIFIC_ATTRIBUTES",
        "ProjectionExpression": "PK,snapshot_id",
    }
