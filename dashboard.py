import streamlit as st
import pandas as pd
import logging
import altair as alt
import matplotlib.pyplot as plt
import os
import csv
from data import parse_logs,fetch_data,separate_manual,load_data,signed_lost,count_reasons,format_stats,not_in,convert_df
from datetime import date, timedelta
from env import LEADS_LOG_GROUP, LEADS_QUERY, NO_QUERY, YES_QUERY,YESNO_LOG_GROUP,REJECTION_REASONS
from streamlit.proto.DataFrame_pb2 import DataFrame
from streamlit.script_request_queue import RerunData
from streamlit.script_runner import StopException, RerunException
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


chart_data=[]
column=[]
@st.cache
def extract(count_data: DataFrame,obj=None,yes=False):
    if yes:
        if obj:
            return  count_data.groupby(["track_popularity"]).size()
        return count_data.groupby(["artist_popularity"]).size()
    else:
        if obj:
            return  count_data.loc[count_data['reason'].isin(options)].groupby(["track_popularity"]).size()
        return count_data.loc[count_data['reason'].isin(options)].groupby(["artist_popularity"]).size()

#####################################################################################
# User Input data range
#####################################################################################
st.text('Choose a date range')
st.session_state["d1"]=date.today()-timedelta(days=7)
st.session_state["d2"]=date.today()
with st.form(key="Date Selector"):
    c1,c2 = st.columns(2)
    st.session_state.d1 = c1.date_input(
     "Start Date",
     st.session_state.d1)
    st.session_state.d2 = c2.date_input(
        "End Date",
        st.session_state.d2)
    submit_button = st.form_submit_button(label='Search')
    if submit_button:
        RerunData()
st.write("Anlayzing dates:   "+ str(st.session_state.d1)+"  --->  "+str(st.session_state.d2))
# data_load_state = st.text('Loading data...')
######################################################################################
# CSS
##########################################################################################
st.markdown(
        f"""
<style>
    .reportview-container .main .block-container{{
        max-width: 1200px;
        padding-top: 0rem;
        padding-right: 0rem;
        padding-left:0rem;
        padding-bottom: 0rem;
    }}
    .reportview-container .main {{
        color: black;
        background-color: white;
    }}
</style>
""",
        unsafe_allow_html=True,
    )
#######################################################
# GET Leads,suitable,Unsiotable counts for this week and last week
###################################################
#LEADS
outdir = './{today}'.format(today=str(st.session_state.d1)+"_"+str(st.session_state.d2))
cached=False
if os.path.exists(outdir):
    cached=True
else:
    os.mkdir(outdir)

leads_delivered = parse_logs(LEADS_QUERY,st.session_state.d1,st.session_state.d2,LEADS_LOG_GROUP)
leads_delivered_tech, leads_delivered_manual = separate_manual(fetch_data(leads_delivered))
leads_delivered_lw = parse_logs(LEADS_QUERY,st.session_state.d1-timedelta(days=7),st.session_state.d2-timedelta(days=7),LEADS_LOG_GROUP)
leads_delivered_lw_tech, leads_delivered_lw_manual = separate_manual(fetch_data(leads_delivered_lw))

#YES's
suitable_lw = fetch_data(parse_logs(YES_QUERY,st.session_state.d1-timedelta(days=7),st.session_state.d2-timedelta(days=7),YESNO_LOG_GROUP))
suitable_tech_lw, suitable_manual_lw = separate_manual(suitable_lw)
if cached is False:
    suitable = parse_logs(YES_QUERY,st.session_state.d1,st.session_state.d2,YESNO_LOG_GROUP)
    suitable_tech, suitable_manual = separate_manual(fetch_data(suitable))
    suitable_tech_df = load_data(suitable_tech)
    suitable_manual_df = load_data(suitable_manual)
    outname = 'suitable.csv'
    full_path = os.path.join(outdir, outname)
    pd.concat([suitable_tech_df,suitable_manual_df]).to_csv(full_path)
else:
    with open('{today}/suitable.csv'.format(today=str(st.session_state.d1)+"_"+str(st.session_state.d2)), encoding="utf-8") as f:
        suitable = [{k: v for k, v in row.items()} for row in csv.DictReader(f, skipinitialspace=True)]
    # suitable = open('/{today}/suitable.csv'.format(today=date.today()))
    suitable_tech, suitable_manual = separate_manual(fetch_data(suitable,id="artist"))
    suitable_tech_df = load_data(suitable_tech)
    suitable_manual_df = load_data(suitable_manual)
#NO's
unsuitable_lw = parse_logs(NO_QUERY,st.session_state.d1-timedelta(days=7),st.session_state.d2-timedelta(days=7),YESNO_LOG_GROUP)
if cached is False:
    unsuitable = fetch_data(parse_logs(NO_QUERY,st.session_state.d1,st.session_state.d2,YESNO_LOG_GROUP),"prospect_status, artist, artist_name, score_data, distributed_country, label_type, microgenre, label")
    unsuitable_df = load_data(unsuitable)
    outname = 'unsuitable.csv'
    full_path = os.path.join(outdir, outname)
    unsuitable_df.to_csv(full_path)
else:
    with open('{today}/unsuitable.csv'.format(today=str(st.session_state.d1)+"_"+str(st.session_state.d2)), encoding="utf-8") as f:
        unsuitable = [{k: v for k, v in row.items()} for row in csv.DictReader(f, skipinitialspace=True)]
    # suitable = open('/{today}/suitable.csv'.format(today=date.today()))
    unsuitable_df = load_data(unsuitable)
    unsuitable_df[["artist_popularity","track_popularity","insta_engagement_rate","monthly_listeners","spot_followers"]] = unsuitable_df[["artist_popularity","track_popularity","insta_engagement_rate","monthly_listeners","spot_followers"]].apply(pd.to_numeric)

#TOTAL REVIEWED
reviewed = suitable_tech+unsuitable
reviewed_lw = suitable_tech_lw+unsuitable_lw
#ACCEPTANCE RATE
acceptance_rate = 100*(len(suitable_tech)/len(reviewed))
acceptance_rate_lw = 100*(len(suitable_tech_lw)/len(reviewed_lw))
# CLOSED & WON
closed_lost,deal_won = signed_lost(st.session_state.d1,st.session_state.d2)
closed_lost_lw,deal_won_lw = signed_lost(st.session_state.d1-timedelta(days=7),st.session_state.d2-timedelta(days=7))



# suitable_tech_df.to_csv('/{today}/suitable.csv'.format(today=date.today()), index=True, mode='w')    
###################################################################################
#METRICS
######################################################################################
a1, a2, a3, a4, a5, a6, a7, a8 = st.columns(8)
a1.metric(label="Leads Delivered", value=len(leads_delivered_tech), delta=str(len(leads_delivered_tech)-len(leads_delivered_lw_tech)))
a3.metric(label="Unsuitable Leads (No)", value=len(unsuitable), delta=str(len(unsuitable)-len(unsuitable_lw)))
a4.metric(label="Suitable Leads (Yes)", value=len(suitable_tech), delta=str(len(suitable_tech)-len(suitable_tech_lw)))
a2.metric(label="Leads Reviewed", value=len(reviewed), delta=str(len(reviewed)-len(reviewed_lw)))
a5.metric(label="Acceptance Rate", value=str(acceptance_rate) + " %", delta=str(acceptance_rate-acceptance_rate_lw))
a6.metric(label="Manual Leads", value=len(suitable_manual), delta=str(len(suitable_manual)-len(suitable_manual_lw)))
a7.metric(label="Closed Lost", value=len(closed_lost), delta=str(len(closed_lost)-len(closed_lost_lw)))
a8.metric(label="Deal Signed", value=len(deal_won), delta=str(len(deal_won)-len(deal_won_lw)))

##############################################################################################
#Analyze Unuitable Leads
#################################################################################################
st.subheader('Analyze Unsuitable Leads')

#################################################################################################
# Negative Reason Chart 
#################################################################################################
source = pd.DataFrame({
    'count': count_reasons(unsuitable_df),
    'reasons': REJECTION_REASONS
})
c=alt.Chart(source).mark_bar().encode(
    x='count',
    y='reasons',
    tooltip=['reasons','count']
)
st.altair_chart(c, use_container_width=True)

#################################################################################################
# Pie charts
#################################################################################################
b1, b2, b3= st.columns(3)
# Pie chart, where the slices will be ordered and plotted counter-clockwise:
labels = list(unsuitable_df['distributed_country'].dropna().unique())[:15]
sizes = list(unsuitable_df["distributed_country"].value_counts(normalize=True) * 100)[:15]
explode =None  # only "explode" the 2nd slice (i.e. 'Hogs')

fig, ax = plt.subplots()
ax.pie(sizes, explode=explode, labels=labels, autopct='%.1f%%',
        shadow=True, startangle=90,radius=.90)
ax.axis('equal')  

b1.text('Country')
b1.pyplot(fig)

# Pie chart, where the slices will be ordered and plotted counter-clockwise:
labels1 = list(unsuitable_df['microgenre'].dropna().unique())[:15]
sizes1 = list(unsuitable_df["microgenre"].value_counts(normalize=True) * 100)[:15]
df = suitable_tech_df.loc[suitable_tech_df['status'].isin(["Currently Tracking"])]
suitable_list = suitable_tech

labels4 = list(df['microgenre'].dropna().unique())[:10]
sizes4 = list(df["microgenre"].value_counts(normalize=True) * 100)[:10]

fig1, ax1 = plt.subplots()
ax1.pie(sizes1, explode=not_in(labels1,labels4), labels=labels1, autopct='%.1f%%',
        shadow=True, startangle=90,radius=.75)
ax1.axis('equal')
b2.text('Microgenre')
b2.pyplot(fig1)

labels2 = list(unsuitable_df['label_type'].dropna().unique())[:15]
sizes2 = list(unsuitable_df["label_type"].value_counts(normalize=True) * 100)[:15]

fig2, ax2 = plt.subplots()
ax2.pie(sizes2, explode=explode, labels=labels2, autopct='%.1f%%',
        shadow=True, startangle=90,radius=.75)
ax2.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
b3.text('Label type')
b3.pyplot(fig2)

#################################################################################################
# Reason Selector
#################################################################################################
st.session_state["y1"] = ["Genre not a fit","Back Catalogue Unavailable"]
st.session_state["y2"] = ["artist","artist_name","label","social_score","spotify_score"]
with st.form(key="Selecting reasons"):
    st.session_state.y1 = st.multiselect(
    'Select reason(s) for unsuitability to analyze',
    REJECTION_REASONS,
    st.session_state.y1)
    if len(st.session_state.y1) == 0:
        options=REJECTION_REASONS
    else:
        options = st.session_state.y1
    unsuitable_df=unsuitable_df.loc[unsuitable_df['reason'].isin(options)]
    st.session_state.y2 = st.multiselect(
        'Select columns(s) to analyze',
        unsuitable_df.columns,
        st.session_state.y2)
    if len(st.session_state.y2) == 0:
        options2=REJECTION_REASONS
    else:
        options2 = st.session_state.y2
    button1, button2 = st.columns(2)
    submit_button = st.form_submit_button(label='Reload Data')
    st.write(unsuitable_df[options2])
    if submit_button:
        RerunData()
st.download_button(
    data=convert_df(unsuitable_df[options2]),
    file_name ="unsuitable_leads.csv",
    mime="text/csv",
    label="Download CSV")
#################################################################################################
# Track/ Artist Popularity Charts
#################################################################################################
c1,c2,c3 = st.columns(3)

popularity = pd.DataFrame({
    'track popularity': extract(unsuitable_df,"track").to_frame().index.values,
    'number of leads': extract(unsuitable_df,"track"),

})
popularity_chart=alt.Chart(popularity).mark_bar().encode(
    x='track popularity',
    y='number of leads',
    tooltip=['track popularity','number of leads']
)
mean_line = alt.Chart(popularity).mark_rule(color='red').encode(
    x='mean(track popularity)',
)
chart_mean = (popularity_chart+mean_line).properties(width=500, height=300).configure_axisX(labelAngle=0)



popularity1 = pd.DataFrame({
    'artist popularity': extract(unsuitable_df).to_frame().index.values,
    'number of leads': extract(unsuitable_df),

})
popularity_chart1=alt.Chart(popularity1).mark_bar().encode(
    x='artist popularity',
    y='number of leads',
    tooltip=['artist popularity','number of leads']
)
mean_line1 = alt.Chart(popularity1).mark_rule(color='green').encode(
    x='mean(artist popularity)',
)
chart_mean1 = (popularity_chart1+mean_line1).properties(width=500, height=300).configure_axisX(labelAngle=0)



popularity_chart2=alt.Chart(unsuitable_df).mark_line(point=True).encode(
    y=alt.Y('artist_popularity',scale=alt.Scale(reverse=False)),
    x='track_popularity',
    color='reason',
    tooltip=['track_popularity','artist_popularity','reason','artist_name'],
)
mean_line2 = alt.Chart(unsuitable_df).mark_rule(color='red').encode(
    x='mean(track_popularity)',
)
mean_line3 = alt.Chart(unsuitable_df).mark_rule(color='green').encode(
    y='mean(artist_popularity)',
)
chart_mean2 = (popularity_chart2+mean_line2+mean_line3).properties(width=500, height=300).configure_axisX(labelAngle=0)

c1.altair_chart(chart_mean, use_container_width=True)
c2.altair_chart(chart_mean1, use_container_width=True)
c3.altair_chart(chart_mean2)


########################################################################
# Charts (Spotify/Social Score; Engagement/Followes; Monthly Listeners/Spotify Followers)
########################################################################
d1,d2 = st.columns(2)
social_breakdown = pd.DataFrame(format_stats(unsuitable_df.to_dict('records')))
chart1=alt.Chart(social_breakdown).mark_circle().encode(
    x='engagement',
    y=alt.Y('followers',scale=alt.Scale(reverse=False)),
    color='platform',
    tooltip=['platform','engagement','followers',"artist_name"]
)
mean_line14 = alt.Chart(social_breakdown).mark_rule(color='brown').encode(
    x='mean(engagement)',
)
mean_line15 = alt.Chart(social_breakdown).mark_rule(color='orange').encode(
    y='mean(followers)'
)
chart_mean9 = (chart1+mean_line14+mean_line15).properties(width=500, height=400 )


chart2=alt.Chart(unsuitable_df).mark_line(point=True).encode(
    x='monthly_listeners',
    y=alt.Y('spot_followers',scale=alt.Scale(reverse=False)),
    color='reason',
    strokeDash='reason',
    tooltip=['monthly_listeners','spot_followers','reason','artist_name']
    )
mean_line16 = alt.Chart(unsuitable_df).mark_rule(color='black').encode(
    x='mean(monthly_listeners)',
)
mean_line17 = alt.Chart(unsuitable_df).mark_rule(color='orange').encode(
    y='mean(spot_followers)'
)
chart_mean10 = (chart2+mean_line17+mean_line16).properties(width=800, height=400).configure_axisX(labelAngle=45)

d1.altair_chart(chart_mean10)
d2.altair_chart(chart_mean9)


###############################################################################
# Analyze Suitable Leads
################################################################################
# b1=alt.Chart(unsuitable_df).mark_bar(point=True).encode(
#     x='spotify_score',
#     y='social_score',
#     Orientation='artist_popularity',
#     color='reason',
#     strokeDash='reason',
#     tooltip=['spotify_score','reason','social_score']
# )

st.subheader('Analyse "Currently Tracking"(Yes) Leads')

if st.checkbox('Include manually sourced leads?'):
    suitable_tech_df =pd.concat([suitable_tech_df,suitable_manual_df]) 
    df = suitable_tech_df.loc[suitable_tech_df['status'].isin(["Currently Tracking"])]
    suitable_list = suitable_tech+suitable_manual
st.session_state["y3"] = ["artist","artist_name","track_name","spotify_score","social_score"]
with st.form(key="Selecting columns"):
    st.session_state.y3 = st.multiselect(
    'Select columns(s) to analyze',
    df.columns,
    st.session_state.y3)
    if len(st.session_state.y3) == 0:
        options3=df.columns
    else:
        options3 = st.session_state.y3
    submit_button = st.form_submit_button(label='Reload Data')
    st.write(df[options3])
    if submit_button:
        RerunData()
st.download_button(
    data=convert_df(df[options3]),
    file_name ="suitable_leads.csv",
    mime="text/csv",
    label="Download CSV")
###############################################################################
# Pie charts
################################################################################
e1,e2, e3= st.columns(3)

labels6 = list(df['distributed_country'].dropna().unique())[:10]
sizes6 = list(df["distributed_country"].value_counts(normalize=True) * 100)[:10]
explode = None  # only "explode" the 2nd slice (i.e. 'Hogs')

fig6, ax6 = plt.subplots()
ax6.pie(sizes6, explode=explode, labels=labels6, autopct='%.1f%%',
        shadow=True, startangle=90,radius=.75)
ax6.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
e1.text('Country')
e1.pyplot(fig6)


explode =None  # only "explode" the 2nd slice (i.e. 'Hogs')

fig4, ax4 = plt.subplots()
ax4.pie(sizes4, explode=not_in(labels4,labels1), labels=labels4, autopct='%.1f%%',
        shadow=True, startangle=90,radius=.90)
ax4.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

e2.text('Microgenre')
e2.text('exploded slices indicate microgenre is also present in Accepted Leads')
e2.pyplot(fig4)

# # Pie chart, where the slices will be ordered and plotted counter-clockwise:
labels5 = list(df['origin'].dropna().unique())[:10]
sizes5 = list(df["origin"].value_counts(normalize=True) * 100)[:10]
explode = None  # only "explode" the 2nd slice (i.e. 'Hogs')

fig5, ax5 = plt.subplots()
ax5.pie(sizes5, explode=explode, labels=labels5, autopct='%.1f%%',
        shadow=True, startangle=90,radius=.75)
ax5.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
e3.text('Lead Origin')
e3.pyplot(fig5)

###############################################################################
# Track/Artist Popularity
################################################################################
f1,f2,f3 = st.columns(3)
pop3 = pd.DataFrame({
    'track popularity': extract(suitable_tech_df,"track",True).to_frame().index.values,
    'number of leads': extract(suitable_tech_df,"track",True),

})
popularity_chart3=alt.Chart(pop3).mark_bar().encode(
    x='track popularity',
    # x2='artist popularity',
    y='number of leads',
    tooltip=['track popularity','number of leads']
)
mean_line4 = alt.Chart(pop3).mark_rule(color='red').encode(
    x='mean(track popularity)',
)
chart_mean3 = (popularity_chart3+mean_line4).properties(width=500, height=300).configure_axisX(labelAngle=0)



pop4 = pd.DataFrame({
    'artist popularity': extract(suitable_tech_df, obj=None, yes=True).to_frame().index.values,
    'number of leads': extract(suitable_tech_df, obj=None, yes=True),

})
popularity_chart4=alt.Chart(pop4).mark_bar(orient="vertical").encode(
    x=alt.X('artist popularity'),
    y='number of leads',
    tooltip=['artist popularity','number of leads']
)
mean_line5 = alt.Chart(pop4).mark_rule(color='green').encode(
    x='mean(artist popularity)',
)
chart_mean4 = (popularity_chart4+mean_line5).properties(width=500, height=300).configure_axisX(labelAngle=0)





popularity_chart5=alt.Chart(df).mark_line(point=True).encode(
    y=alt.Y('artist_popularity',scale=alt.Scale(reverse=True)),
    x='track_popularity',
    tooltip=['track_popularity','artist_popularity','reason','artist_name'],
)
mean_line6 = alt.Chart(df).mark_rule(color='red').encode(
    x='mean(track_popularity)',
)
mean_line7 = alt.Chart(df).mark_rule(color='green').encode(
    y='mean(artist_popularity)'
)
chart_mean5 = (popularity_chart5+mean_line7+mean_line6).properties(width=500, height=300).configure_axisX(labelAngle=0)


f1.altair_chart(chart_mean3, use_container_width=True)
f2.altair_chart(chart_mean4, use_container_width=True)
f3.altair_chart(chart_mean5)

###############################################################################
# Social, Spotify CHARTS
###############################################################################
g1, g2= st.columns(2)

social_suitable = pd.DataFrame(format_stats(suitable_list))
chart3=alt.Chart(social_suitable).mark_circle(point=True,orient="vertical").encode(
    x='engagement',
    y=alt.Y('followers',scale=alt.Scale(reverse=False)),
    color='platform',
    # strokeDash='reason',
    tooltip=["engagement","platform","followers","artist_name"]

)
mean_line8 = alt.Chart(social_suitable).mark_rule(color='brown').encode(
    x='mean(engagement)',
)
mean_line9 = alt.Chart(social_suitable).mark_rule(color='orange').encode(
    y='mean(followers)'
)
chart_mean6 = (chart3+mean_line8+mean_line9).properties(width=700, height=300).configure_point(
    size=500)



chart4=alt.Chart(df).mark_line(point=True).encode(
    x='monthly_listeners',
    y=alt.Y('spot_followers', scale=alt.Scale(reverse=False)),
    # color='reason',
    tooltip=["monthly_listeners","spot_followers","artist_name"]
)
mean_line10 = alt.Chart(df).mark_rule(color='black').encode(
    x='mean(monthly_listeners)',
)
mean_line11 = alt.Chart(df).mark_rule(color='orange').encode(
    y='mean(spot_followers)'
)
chart_mean7 = (chart4+mean_line10+mean_line11).properties(width=700, height=300).configure_axisX(labelAngle=45)

chart5=alt.Chart(df).mark_line(point=True).encode(
    x='social_score',
    y=alt.Y('spotify_score'),
    tooltip=["social_score","spotify_score","artist_name"]
)
mean_line12 = alt.Chart(df).mark_rule(color='purple').encode(
    x='mean(social_score)',
)
mean_line13 = alt.Chart(df).mark_rule(color='yellow').encode(
    y='mean(spotify_score)'
)
chart_mean8 = (chart5+mean_line12+mean_line13).properties(width=700, height=300)
g1.altair_chart(chart_mean6, use_container_width=True)
g2.altair_chart(chart_mean7, use_container_width=True)
st.altair_chart(chart_mean8, use_container_width=True)

